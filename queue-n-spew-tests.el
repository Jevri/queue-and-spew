;;; queue-n-spew-tests.el --- Tests for Queue and Spew mode -*- lexical-binding: t -*-

;; Copyright (C) 2023 Viktor Obeling

;; Author: Viktor Obeling <viktor@obeling.org>
;; URL: https://gitlab.com/Jevri/queue-and-spew

;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation; either version 3, or (at
;; your option) any later version.

;; This program is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file COPYING.  If not, write to the
;; Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;; Boston, MA 02111-1307, USA.

;;; Code:
(require 'ert)
(require 'queue-n-spew)

(ert-deftest queue-and-spew-should-yank-kill-ring-in-order ()
  (with-temp-buffer
    (queue-and-spew-mode)
    (kill-new "abc")
    (kill-new "123")
    (kill-new "xyz")

    (yank)
    (insert "\n")
    (yank)
    (insert "\n")
    (yank)

    (should (string= (buffer-string) "abc
123
xyz"))
    (queue-and-spew-mode -1)))

(ert-deftest queue-and-spew-should-start-at-mode-activation ()
  (with-temp-buffer
    (kill-new "abc")
    (queue-and-spew-mode)
    (kill-new "123")
    (kill-new "xyz")

    (yank)
    (insert "\n")
    (yank)

    (should (string= (buffer-string)
                     "123
xyz"))
    (queue-and-spew-mode -1)))

(ert-deftest queue-and-spew-should-use-last-kill-at-mode-deactivation ()
  (with-temp-buffer
    (queue-and-spew-mode)
    (kill-new "abc")
    (kill-new "123")
    (kill-new "xyz")
    (queue-and-spew-mode -1)
    
    (yank)

    (should (string= (buffer-string) "xyz"))))

(ert-deftest queue-and-spew-should-yank-all-when-mixed-with-kill ()
  (with-temp-buffer
    (queue-and-spew-mode)
    (kill-new "abc")
    (kill-new "123")
    (yank)
    (insert "\n")

    (kill-new "xyz")
    (yank)
    (insert "\n")
    (yank)

    (should (string= (buffer-string)
                     "abc
123
xyz"))
    (queue-and-spew-mode -1)))

(ert-deftest queue-and-spew-should-report-error-when-queue-is-empty ()
  (with-temp-buffer
    (queue-and-spew-mode)
    (kill-new "abc")
    (yank)

    (should-error (yank) :type 'error)
    (queue-and-spew-mode -1)))

(ert-deftest queue-and-spew-should-queue-interprogram-paste ()
  (with-temp-buffer
    (let* ((save-interprogram-paste-before-kill nil) ; only save when yanking
           pasted
           (interprogram-paste-function (lambda () (unless pasted (setq pasted t) "123"))))
      (queue-and-spew-mode)
      (kill-new "abc")

      (yank)
      (insert "\n")
      (yank)

      (should (string= (buffer-string) "abc
123"))
      (queue-and-spew-mode -1))))

(ert-deftest queue-and-spew-should-yank-multi-select-interprogram-paste-in-order ()
  (with-temp-buffer
    (let* ((save-interprogram-paste-before-kill nil) ; only save when yanking
           pasted
           (interprogram-paste-function (lambda () (unless pasted (setq pasted t)
                                                           (list "abc" "123")))))
      (queue-and-spew-mode)

      (yank)
      (insert "\n")
      (yank)

      (should (string= (buffer-string) "abc
123"))
      (queue-and-spew-mode -1))))

(ert-deftest queue-and-spew-should-yank-first-multi-select-interprogram-paste-item-after-disabling-queue-and-spew-mode ()
  (with-temp-buffer
    (let* ((save-interprogram-paste-before-kill nil) ; only save when yanking
           pasted
           (interprogram-paste-function (lambda () (unless pasted (setq pasted t)
                                                           (list "abc" "123")))))
      (queue-and-spew-mode)
      (yank)
      (queue-and-spew-mode -1)
      (insert "\n")

      (yank)

      (should (string= (buffer-string) "abc
abc")))))

(provide 'queue-n-spew-tests)
;;; queue-n-spew-tests.el ends here
