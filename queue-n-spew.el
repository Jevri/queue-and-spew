;;; queue-n-spew.el --- Yank killed text in order -*- lexical-binding: t -*-

;; Copyright (C) 2020, 2023 Viktor Obeling

;; Author: Viktor Obeling <viktor@obeling.org>
;; Version: 1.0
;; Keywords: convenience, killing, kill-ring, yank
;; URL: https://gitlab.com/Jevri/queue-and-spew

;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation; either version 3, or (at
;; your option) any later version.

;; This program is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs; see the file COPYING.  If not, write to the
;; Free Software Foundation, Inc., 59 Temple Place - Suite 330,
;; Boston, MA 02111-1307, USA.

;;; Commentary:

;; Reduce switching between buffers by killing text in one face and yanking in an other.

;; The package works by turning the kill ring into an advancing queue instead
;; of always yanking the last thing killed.

;; To use this queue your kills type `M-x queue-n-spew-mode`.
;; When you are done killing and yankng your things type `M-x queue-n-spew-mode` again.

;;; Code:
(defvar qns--marker nil "Marks the end of kills to be queued.")
(defvar qns-kill-ring nil)

(defun qns--reverse-kill-ring ()
  "Reverse the kill ring until marker, then append the reveresed list."
  (let ((next kill-ring)
        (queue nil))
    (while (not (eq qns--marker next))
      (push (car next) queue)
      (setq next (cdr next)))
    (setq qns--marker kill-ring)
    (setq qns-kill-ring (append qns-kill-ring queue))))

(defun qns--queue-interprogram-paste ()
  "Queue the interprogram paste."
  (let ((interprogram-paste (if interprogram-paste-function
                                (funcall interprogram-paste-function))))
    (when interprogram-paste
      ;; Disable the interprogram cut function when we add the new
      ;; text to the kill ring, so Emacs doesn't try to own the
      ;; selection, with identical text.
      (let ((interprogram-cut-function nil))
	(if (listp interprogram-paste)
            (progn
              (let ((kill-ring nil)
                    (qns--marker nil))
                (mapc #'kill-new interprogram-paste)
                (qns--reverse-kill-ring))
              (mapc #'kill-new (reverse interprogram-paste))
	      (setq qns--marker kill-ring))
	  (kill-new interprogram-paste))))))

;;;###autoload
(define-minor-mode queue-and-spew-mode
  "Yank killed text in order"
  :global t
  :lighter " queue-n-spew"
  (if queue-and-spew-mode
      (progn
        (advice-add 'yank :around 'qns-yank)
        (setq qns--marker kill-ring))
    (advice-remove 'yank 'qns-yank)
    (setq qns--marker nil)
    (setq qns-kill-ring nil)))

(defun qns-yank (yank &rest arg)
  "Wrapper around `yank' to reorganize the kill ring.
YANK should be the `yank' symbol. ARG is the arguments for yank."
  (qns--queue-interprogram-paste)
  (qns--reverse-kill-ring)
  (let ((kill-ring qns-kill-ring)
        (kill-ring-yank-pointer qns-kill-ring))
    (apply yank arg))
  (pop qns-kill-ring))

(provide 'queue-n-spew)
;;; queue-n-spew.el ends here
